package zenika.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class BeansConfiguration {

    @Bean
    Random random(){
        return new Random();
    }
}
