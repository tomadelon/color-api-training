#première image
FROM maven:3.8.1-openjdk-17 AS builder
WORKDIR /app
COPY pom.xml .
RUN mvn -e -B dependency:resolve
COPY src ./src
RUN mvn clean -e -B package
# RTSDK Java : deuxième image
FROM openjdk:slim
WORKDIR /app
COPY --from=builder /app/target/color-api-1.0-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "./color-api-1.0-SNAPSHOT.jar"]